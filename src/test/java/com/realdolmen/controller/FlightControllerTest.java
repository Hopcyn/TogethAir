package com.realdolmen.controller;

import com.realdolmen.domain.Flight;
import com.realdolmen.service.FlightService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@Ignore
@RunWith(MockitoJUnitRunner.class)
public class FlightControllerTest {

	@Mock
	FlightService flightService;

	@InjectMocks
	FlightController flightController;

	private Flight flight;

	@Before
	public void init()
	{
		flight = new Flight();
		flight.setEnabled(true);
	}


	@Test
	public void getFlight()
	{
		Mockito.when(flightService.findById(1L)).thenReturn(flight);
		flightController = new FlightController();
	}

}
