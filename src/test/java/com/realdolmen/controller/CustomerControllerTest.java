package com.realdolmen.controller;

import com.realdolmen.domain.Customer;
import com.realdolmen.service.CustomerService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(MockitoJUnitRunner.class)
public class CustomerControllerTest {

	@Mock
	CustomerService customerService;

	@InjectMocks
	CustomerController customerController = new CustomerController();

	private Customer c = new Customer();
	private List<Customer> customers = new ArrayList<>();


	@Before
	public void init()
	{
		c.setEmailAddress("test@test.com");
		c.setFirstName("sam");
		customers.add(c);
	}

	@Test
	public void listNotEmptyAfterCreate()
	{

		Mockito.when(customerService.getAllCustomers()).thenReturn(customers);
		assertNotNull(customerController.getCustomerList());
		assertEquals("test@test.com",c.getEmailAddress());
	}

	@Ignore
	@Test
	public void completeTheme()
	{
		Mockito.when(customerService.getAllCustomers()).thenReturn(customers);
		List<Customer> returnedLijst = customerController.completeTheme("sam");
		assertEquals(c,returnedLijst.get(1));
	}
}
