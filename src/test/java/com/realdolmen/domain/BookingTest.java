package com.realdolmen.domain;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.annotation.PostConstruct;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class BookingTest {

	Booking booking;

	@Before
	public void init()
	{
		booking = new Booking(1, new BigDecimal(5), "firstClass", "CONFIRMED", true, new Partner(), new Flight(), "creditcard", new PaymentMethod() {});
	}


	@Test
	public void getSeats()
	{
		assertEquals(1,booking.getAmountOfSeats());
	}

	@Test
	public void getPrice()
	{
		assertEquals(new BigDecimal(5),booking.getTotalPrice());
	}

	@Test
	public void setPrice()
	{
		Booking b = new Booking();
		b.setTotalPrice(new BigDecimal(60));
		assertEquals(new BigDecimal(60),b.getTotalPrice());
	}

	@Test
	public void getClassType()
	{
		assertEquals("firstClass",booking.getTravelingClass());
	}
	@Test
	public void setClassType()
	{
		Booking b = new Booking();
		b.setTravelingClass("first");
		assertEquals("first",b.getTravelingClass());
	}

	@Test
	public void getStatus()
	{
		assertEquals("CONFIRMED",booking.getStatus());
	}

	@Test
	public void setStatus()
	{
		Booking b = new Booking();
		b.setStatus("pending");
		assertEquals("pending",b.getStatus());
	}

	@Test
	public void getPaymentMethod()
	{
		assertEquals("creditcard",booking.getPaymentMethodType());
	}

	@Test
	public void setPaymentMethod()
	{
		Booking b = new Booking();
		b.setPaymentMethodType("credit");
		assertEquals("credit",b.getPaymentMethodType());
	}

	@Test
	public void getEnabled()
	{
		assertEquals(true,booking.getEnabled());
	}
	@Test
	public void setEnabled()
	{
		Booking b = new Booking();
		b.setEnabled(false);
		assertEquals(false,b.getEnabled());
	}

	@Test
	public void setSeats()
	{
		Booking b = new Booking();
		b.setAmountOfSeats(5);
		assertEquals(5,b.getAmountOfSeats());
	}

}
