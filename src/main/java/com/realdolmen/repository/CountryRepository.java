package com.realdolmen.repository;


import com.realdolmen.domain.Booking;
import com.realdolmen.domain.Country;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class CountryRepository {

	@PersistenceContext
	EntityManager em;

	public List<Country> getAllCountries(){
		return em.createQuery("select p from Country p", Country.class).getResultList();
	};

}
