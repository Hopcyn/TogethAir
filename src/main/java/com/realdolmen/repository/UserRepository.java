package com.realdolmen.repository;


import com.realdolmen.domain.Customer;
import com.realdolmen.domain.User;
import com.realdolmen.security.BCryptEncoder;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.function.Function;

@Stateless
public class UserRepository {

	@PersistenceContext
	EntityManager em;

	private static final BCryptEncoder bCryptEncoder = new BCryptEncoder(11);

	public List<User> getAllUsers()
	{
		return em.createQuery("select p from User p", User.class).getResultList();
	}

	public User getUserByCredentials(User user){
		String[] mutableHash = new String[1];
		Function<String, Boolean> update = hash -> { mutableHash[0] = hash; return true; };

		User foundUser = null;
		try{
			foundUser = em.createQuery("select p from User p where p.emailAddress = :emailAddress", User.class)
					.setParameter("emailAddress", user.getEmailAddress().toLowerCase())
					.getSingleResult();
		} catch(Exception e){
			System.out.println(e.getMessage());
		}

		if(foundUser != null){
				return bCryptEncoder.verifyAndUpdateHash(user.getPassword(), foundUser.getPassword(), update) ? foundUser : null;
		}
		return null;
	}

	public void persist(User user){
		user.setEmailAddress(user.getEmailAddress().toLowerCase());
		user.setPassword(bCryptEncoder.hash(user.getPassword()));
		em.persist(user);}

}
