package com.realdolmen.repository;

import com.realdolmen.domain.Location;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


@Stateless
public class LocationRepository {
	@PersistenceContext
	EntityManager em;

	public List<Location> getAllLocations(){
		return em.createQuery("select p from Location p", Location.class).getResultList();

	}

	public List<Location> getAllActiveLocations(){
		return em.createQuery("select p from Location p where enabled=true", Location.class).getResultList();

	}

	public Location getLocationByCode(String code)
	{
		try {
			return em.createQuery("select p from Location p where p.enabled=true and p.airportCode=:code", Location.class)
					.setParameter("code", code)
					.getResultList().get(0);
		}catch(IndexOutOfBoundsException e)
		{
			return null;
		}
	}

	public void persist(Location location){
		em.persist(location);
	}

	public void merge(Location location){
		em.merge(location);
	}
}
