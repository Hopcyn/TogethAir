package com.realdolmen.repository;


import com.realdolmen.domain.Customer;
import com.realdolmen.domain.Flight;
import com.realdolmen.domain.User;
import com.realdolmen.dto.UserDTO;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class CustomerRepository {

	@PersistenceContext
	EntityManager em;

	public List<Customer> getAllCustomers()
	{
		return em.createQuery("select p from Customer p", Customer.class).getResultList();
	}
}
