package com.realdolmen.repository;


import com.realdolmen.domain.Partner;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class PartnerRepository {

	@PersistenceContext
	EntityManager em;

	public List<Partner> getAllPartners(){
		return em.createQuery("select p from Partner p", Partner.class).getResultList();

	}
}
