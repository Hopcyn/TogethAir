package com.realdolmen.repository;

import com.realdolmen.domain.Employee;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class EmployeeRepository {
	@PersistenceContext
	EntityManager em;

	public List<Employee> getAllEmployees()
	{
		return em.createQuery("select p from Employee p", Employee.class).getResultList();

	}
}
