package com.realdolmen.repository;


import com.realdolmen.domain.Booking;
import com.realdolmen.domain.Flight;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.List;

@Stateless
public class BookingRepository {

	@PersistenceContext
	EntityManager em;

	public List<Booking> getAllBookings(){
		return em.createQuery("select p from Booking p", Booking.class).getResultList();
	};

	public void save(Booking booking)
	{
		em.persist(booking);
	}

	public List<Booking> getAllBookingsByStatus(String status){
		return em.createQuery("select p from Booking p where p.status= :status", Booking.class)
				.setParameter("status", status)
				.getResultList();
	}

	public Double getAvgPriceCostFromAllBookings()
	{
		List l = em.createQuery(
				"SELECT AVG(e.totalPrice) FROM Booking e",Double.class)
				.getResultList();
		return (Double) l.get(0);
	}

	public BigDecimal getMinPriceCostFromAllBookings()
	{
		List l = em.createQuery(
				"SELECT MIN(e.totalPrice) FROM Booking e",BigDecimal.class)
				.getResultList();
		return (BigDecimal) l.get(0);
	}
	public BigDecimal getMaxPriceCostFromAllBookings()
	{
		List l = em.createQuery(
				"SELECT MAX(e.totalPrice) FROM Booking e",BigDecimal.class)
				.getResultList();
		return (BigDecimal) l.get(0);
	}

	public List<Booking> getAllBookingsByPartner(String companyName){
		return em.createQuery("select p from Booking p where p.flight.partner.companyName=:companyName", Booking.class)
				.setParameter("companyName",companyName).getResultList();
	};

	public List<Booking> getAllBookingsByStatusByPartner(String status,String companyName){
		return em.createQuery("select p from Booking p where p.status= :status and p.flight.partner.companyName=:companyName", Booking.class)
				.setParameter("status", status).setParameter("companyName",companyName)
				.getResultList();
	}

	public Double getAvgPriceCostFromAllBookingsByPartner(String companyName)
	{
		List l = em.createQuery(
				"SELECT AVG(e.totalPrice) FROM Booking e where e.flight.partner.companyName=:companyName",Double.class).setParameter("companyName",companyName)
				.getResultList();
		return (Double) l.get(0);
	}

	public BigDecimal getMinPriceCostFromAllBookingsByPartner(String companyName)
	{
		List l = em.createQuery(
				"SELECT MIN(e.totalPrice) FROM Booking e where e.flight.partner.companyName=:companyName",BigDecimal.class).setParameter("companyName",companyName)
				.getResultList();
		return (BigDecimal) l.get(0);
	}
	public BigDecimal getMaxPriceCostFromAllBookingsByPartner(String companyName)
	{
		List l = em.createQuery(
				"SELECT MAX(e.totalPrice) FROM Booking e where e.flight.partner.companyName=:companyName",BigDecimal.class).setParameter("companyName",companyName)
				.getResultList();
		return (BigDecimal) l.get(0);
	}
}
