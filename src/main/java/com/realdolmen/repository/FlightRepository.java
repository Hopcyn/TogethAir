package com.realdolmen.repository;

import com.realdolmen.domain.Flight;
import com.realdolmen.domain.Location;
import com.realdolmen.domain.Partner;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Stateless
public class FlightRepository {

	@PersistenceContext
	EntityManager em;



	public List<Flight> getAllFlights()
	{
		return em.createQuery("select p from Flight p", Flight.class).getResultList();
	}

	public List<Flight> getFlightFromToWithDate(Location departure, Location destination, LocalDateTime date)
	{
		return em.createQuery("select p from Flight p where departure_id=:myId and enabled=true and destination_id=:myDestination" +
				" and departureTime between '"+ date.toLocalDate() + "' and '" + date.plusDays(1).toLocalDate()+"'", Flight.class)
				.setParameter("myId", departure.getId())
				.setParameter("myDestination",destination.getId())
				.getResultList();

	}

	public List<Flight> getFlightsByPartnerId(Partner partner){
		return em.createQuery("select p from Flight p where p.partner= :partner", Flight.class)
				.setParameter("partner", partner)
				.getResultList();
	}

	public Flight findById(Long id)
	{
		return em.find(Flight.class,id);
	}

	public void merge(Flight flight){em.merge(flight);}

	public void persist(Flight flight){em.persist(flight);}

}
