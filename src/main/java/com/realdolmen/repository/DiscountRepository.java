package com.realdolmen.repository;


import com.realdolmen.domain.Booking;
import com.realdolmen.domain.Discount;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class DiscountRepository {

	@PersistenceContext
	EntityManager em;

	public void save(Discount discount)
	{
		em.persist(discount);
	}
}
