package com.realdolmen.generator;

import com.realdolmen.bean.NavigationBean;
import com.realdolmen.domain.Booking;
import net.sf.jasperreports.engine.*;
import org.omnifaces.util.Faces;

import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.HashMap;


@RequestScoped
public class ReportGenerator {

	@Inject
	NavigationBean navigationBean;

	public byte[] generate(Booking booking,int discountPercentage){
		String in = "c:/PDF/booking_pdf.jrxml";
		/*InputStream in = getClass().getResourceAsStream("resources/jasper/booking_pdf.jrxml");*/
		String out = "c:/PDF/booking.pdf";

		HashMap<String, Object> hm = new HashMap<String, Object>();

		hm.put("Title","Jouw Boeking");
		hm.put("status",booking.getStatus());
		hm.put("class",booking.getTravelingClass());
		hm.put("seats",booking.getAmountOfSeats());
		hm.put("price",booking.getTotalPrice());
		hm.put("paymentMethod",booking.getPaymentMethodType());
		hm.put("discount",discountPercentage);
		hm.put("departure",booking.getFlight().getDeparture().getAirportName());
		hm.put("destination",booking.getFlight().getDestination().getAirportName());
		hm.put("userName",booking.getUser().getFirstName());
		hm.put("userLastName",booking.getUser().getLastName());
		hm.put("userEmail",booking.getUser().getEmailAddress());



		JasperReport jasperReport = null;
		try {
			jasperReport = JasperCompileManager.compileReport(in);
			// fill report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, hm, new JREmptyDataSource());

			// export report to PDF
			JasperExportManager.exportReportToPdfFile(jasperPrint, out);
			byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);

			return pdf;

		} catch (JRException /*| IOException*/ e) {
			e.printStackTrace();
		}
		return null;
	}
}
