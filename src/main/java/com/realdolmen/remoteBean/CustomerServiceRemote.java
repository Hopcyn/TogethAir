package com.realdolmen.remoteBean;

import com.realdolmen.domain.Customer;
import com.realdolmen.domain.User;

import java.util.List;

public interface CustomerServiceRemote {

	List<Customer> getAllCustomers();
}
