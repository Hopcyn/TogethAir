package com.realdolmen.remoteBean;

import com.realdolmen.domain.Customer;
import com.realdolmen.domain.User;

import java.util.List;

public interface UserServiceRemote {
	List<User> getAllUsers();

	User getUserByCredentials(User user);

	void persist(User user);

}
