package com.realdolmen.remoteBean;

import com.realdolmen.domain.Partner;

import java.util.List;

public interface PartnerServiceRemote {

	List<Partner> getAllPartners();
}
