package com.realdolmen.remoteBean;

import com.realdolmen.domain.Booking;

import java.math.BigDecimal;
import java.util.List;

public interface BookingServiceRemote {
	List<Booking> getAllBookings();
	void save(Booking booking);
	List<Booking> getAllBookingsByStatus(String status);
	Double getAvgPriceCostFromAllBookings();
	BigDecimal getMinPriceCostFromAllBookings();
	BigDecimal getMaxPriceCostFromAllBookings();
	List<Booking> getAllBookingsByPartner(String companyName);
	List<Booking> getAllBookingsByStatusByPartner(String status,String companyName);
	Double getAvgPriceCostFromAllBookingsByPartner(String companyName);
	BigDecimal getMinPriceCostFromAllBookingsByPartner(String companyName);
	BigDecimal getMaxPriceCostFromAllBookingsByPartner(String companyName);
}
