package com.realdolmen.remoteBean;

import com.realdolmen.domain.Location;

import java.util.List;

public interface LocationServiceRemote {

	List<Location> getAllLocations();
	List<Location> getAllActiveLocations();
	void persist(Location location);
	void merge(Location location);
	Location getLocationByCode(String code);

}
