package com.realdolmen.remoteBean;

import com.realdolmen.domain.Booking;
import com.realdolmen.domain.Discount;

import java.util.List;

public interface DiscountServiceRemote {
	void save(Discount discount);
}
