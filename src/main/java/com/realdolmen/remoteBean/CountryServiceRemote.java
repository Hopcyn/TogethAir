package com.realdolmen.remoteBean;

import com.realdolmen.domain.Booking;
import com.realdolmen.domain.Country;

import java.util.List;

public interface CountryServiceRemote {
	List<Country> getAllCountries();
}
