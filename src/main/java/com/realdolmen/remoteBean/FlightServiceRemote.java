package com.realdolmen.remoteBean;


import com.realdolmen.domain.Flight;
import com.realdolmen.domain.Location;
import com.realdolmen.domain.Partner;
import com.realdolmen.domain.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface FlightServiceRemote {

	List<Flight> getAllFlights();
	List<Flight> getFlightFromToWithDate(Location departure, Location destination, LocalDateTime date);
	List<Flight> getFlightsByPartner(Partner partner);
	Flight findById(Long id);
	void merge(Flight flight);
	void persist(Flight flight);
}
