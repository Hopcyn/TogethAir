package com.realdolmen.remoteBean;

import com.realdolmen.domain.Employee;

import java.util.List;

public interface EmployeeServiceRemote {

	List<Employee> getAllEmployees();
}
