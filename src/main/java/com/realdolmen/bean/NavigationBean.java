package com.realdolmen.bean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by RCCBH67 on 12/09/2018.
 */
@Named
@RequestScoped
public class NavigationBean {
    @Inject
    private UserBean userBean;

    private String redirectionString = "?faces-redirect=true";

    public String navigateToHome(Boolean redirect){
        return ("index.xhtml" + (redirect ? this.redirectionString : ""));
    }

    public String navigateToLogin(Boolean redirect){
        return ("login.xhtml" + (redirect ? this.redirectionString : ""));
    }

    public String navigateToFlightOverview(Boolean redirect){
        return ("flightOverview.xhtml" + (redirect ? this.redirectionString : ""));
    }

    public String navigateToConfirmBooking(Boolean redirect){
        return ("confirmBooking.xhtml" + (redirect ? this.redirectionString : ""));
    }
    public String navigateToBookingDetail(Boolean redirect){
        return ("booking.xhtml" + (redirect ? this.redirectionString : ""));
    }

    public String navigateToRegistrationPage(Boolean redirect){
        return ("register.xhtml" + (redirect ? this.redirectionString : ""));
    }

    public String navigateTo404Page(){
        return "404.xhtml";
    }

    public String navigateToBookingSuccess(boolean redirect){
        return ("bookingSuccess.xhtml"+ (redirect ? this.redirectionString : ""));
    }
}
