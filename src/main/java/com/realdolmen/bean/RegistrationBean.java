package com.realdolmen.bean;

import com.realdolmen.domain.Customer;
import com.realdolmen.domain.User;
import com.realdolmen.dto.UserDTO;
import com.realdolmen.service.UserService;
import com.realdolmen.utilities.MailService;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by RCCBH67 on 11/09/2018.
 */
@Named
@RequestScoped
public class RegistrationBean implements Serializable {
    @Inject
    private NavigationBean navigationBean;

    @Inject
    private UserService userService;

    @Inject
    private MailService mailService;

    private Customer user = new Customer();

    private Boolean failedRegistrationAttempt = false;

    private Boolean successfulRegistration = false;

    public RegistrationBean() {
        user = new Customer();
    }

    public Customer getUser() {
        return user;
    }

    public void setUser(Customer user) {
        this.user = user;
    }

    public String register(){
        if(this.userService.getUserByCredentials(this.user) != null) {
            this.failedRegistrationAttempt = true;
        } else {
            this.userService.persist(this.user);
            this.failedRegistrationAttempt= false;
            this.successfulRegistration = true;
            try {
                this.mailService.confirmRegistration(this.user);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.user = new Customer();
        }

        return this.navigationBean.navigateToRegistrationPage(false);
    }

    public Boolean getFailedRegistrationAttempt() {
        return failedRegistrationAttempt;
    }

    public Boolean getSuccessfulRegistration() {
        return successfulRegistration;
    }
}
