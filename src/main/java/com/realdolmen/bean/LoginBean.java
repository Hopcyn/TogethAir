package com.realdolmen.bean;

import com.realdolmen.domain.Customer;
import com.realdolmen.dto.UserDTO;
import com.realdolmen.service.CustomerService;
import com.realdolmen.service.EmployeeService;
import com.realdolmen.service.PartnerService;
import com.realdolmen.service.UserService;
import com.sun.org.apache.xpath.internal.operations.Bool;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by RCCBH67 on 10/09/2018.
 */

@Named
@RequestScoped
public class LoginBean implements Serializable {

    @Inject
    private UserBean userBean;

    @Inject
    private NavigationBean navigationBean;

    @Inject
    private UserService userService;

    @Inject
    private GrowlBean growlBean;

    private Boolean failedLoginAttempt = false;


    public String login() {
        if(!findUser()){
            this.failedLoginAttempt = true;
            return this.navigationBean.navigateToLogin(false);
        }

        return this.navigationBean.navigateToHome(true);
    }

    public String logout() {
        userBean.setUser(new UserDTO());
        return this.navigationBean.navigateToHome(true);
    }


    private Boolean findUser(){
        if(this.userService.getUserByCredentials(userBean.getUser()) != null){
            userBean.setUser(this.userService.getUserByCredentials(userBean.getUser()));
            return true;
        }
        return false;
    }

    public Boolean getFailedLoginAttempt() {
        return failedLoginAttempt;
    }

}
