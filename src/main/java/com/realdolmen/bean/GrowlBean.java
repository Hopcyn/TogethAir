package com.realdolmen.bean;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ResourceBundle;

/**
 * Created by RCCBH67 on 12/09/2018.
 */
@Named
@SessionScoped
public class GrowlBean implements Serializable {
    @Inject
    private LanguageSwitcher languageSwitcher;

    private FacesMessage message = new FacesMessage();

    public void showInformationGrowl(String detail, String summary){
        this.message.setDetail(detail);
        this.message.setSummary(summary);
        this.message.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, this.message);
    }

    public void showFatalGrowl(String detail, String summary){
        this.message.setDetail(detail);
        this.message.setSummary(summary);
        this.message.setSeverity(FacesMessage.SEVERITY_FATAL);
        FacesContext.getCurrentInstance().addMessage(null, this.message);
    }

    public void showErrorGrowl(String detail, String summary){
        this.message.setDetail(detail);
        this.message.setSummary(summary);
        this.message.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, this.message);
    }

    public void showWarningGrowl(String detail, String summary){
        this.message.setDetail(detail);
        this.message.setSummary(summary);
        this.message.setSeverity(FacesMessage.SEVERITY_WARN);
        FacesContext.getCurrentInstance().addMessage(null, this.message);
    }
}
