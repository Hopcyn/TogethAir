package com.realdolmen.bean;

import com.realdolmen.domain.User;
import com.realdolmen.dto.UserDTO;


import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by RCCBH67 on 11/09/2018.
 */
@Named
@SessionScoped
public class UserBean implements Serializable {
    private User user = new UserDTO();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserBean() {
        user = new UserDTO();
    }

    public Boolean isLoggedIn() {
        return this.isCustomer() | this.isEmployee() | this.isPartner();
    }

    public Boolean isCustomer(){
        return this.user.getClass().getSimpleName().equals("Customer");
    }

    public Boolean isEmployee(){
        return this.user.getClass().getSimpleName().equals("Employee");
    }

    public Boolean isPartner(){
        return this.user.getClass().getSimpleName().equals("Partner");
    }
}
