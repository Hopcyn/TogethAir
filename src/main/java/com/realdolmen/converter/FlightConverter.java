package com.realdolmen.converter;

import com.realdolmen.domain.Flight;
import com.realdolmen.service.FlightService;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.lang.annotation.Annotation;

@Named
@SessionScoped
public class FlightConverter implements Converter, Serializable {

	@Inject
	FlightService flightService;

	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
		if (value == null || value.isEmpty()) {
			return null;
		}

		try {
			Long id = Long.valueOf(value);
			return flightService.findById(id);
		} catch (NumberFormatException e) {
			throw new ConverterException("The value is not a valid Flight ID: " + value, e);
		}
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
		if(value == null) {
			return "";
		}

		if (value instanceof Flight) {
			Long id = ((Flight) value).getId();
			return (id != null) ? String.valueOf(id) : null;
		} else {
			throw new ConverterException("The value is not a valid Flight instance: " + value);
		}
	}
}
