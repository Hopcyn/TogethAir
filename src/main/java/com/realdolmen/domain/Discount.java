package com.realdolmen.domain;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
public class Discount implements Serializable {
	@Id
	@GeneratedValue
	private Long id;
	@DecimalMin(value = "1", message = "{Lower_Than_One}")
	@NotNull(message = "{Field_Empty}")
	private int amountOfSeats;
	@DecimalMin(value = "1", message = "{Lower_Than_One}")
	@NotNull(message = "{Field_Empty}")
	private int percentage;
	@ManyToOne
	private Flight flight;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getAmountOfSeats() {
		return amountOfSeats;
	}

	public void setAmountOfSeats(int amountOfSeats) {
		this.amountOfSeats = amountOfSeats;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}
}
