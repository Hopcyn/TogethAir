package com.realdolmen.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
public class Location implements Serializable {

	@Id
	@GeneratedValue
	private Long id;
	@NotNull(message = "{Field_Empty}")
	@NotBlank(message = "{Field_Empty}")
	private String airportName;
	@NotNull(message = "{Field_Empty}")
	@NotBlank(message = "{Field_Empty}")
	private String airportCode;
	private Boolean enabled;
	@OneToOne(cascade = CascadeType.ALL)
	@NotNull(message = "{Field_Not_Selected}")
	private Country country;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String aiportName) {
		this.airportName = aiportName;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return this.getAirportName() + " (" + this.getAirportCode() + ")";
	}


}
