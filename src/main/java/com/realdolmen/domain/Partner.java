package com.realdolmen.domain;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@DiscriminatorValue(value = "PARTNER")
@Entity
public class Partner extends User{
	private String companyName;

	public Partner() {
	}

	public Partner(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
}
