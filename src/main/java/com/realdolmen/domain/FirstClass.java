package com.realdolmen.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@DiscriminatorValue(value = "FIRST_CLASS")
@Entity
public class FirstClass extends FlightClass {


	public String getClassName()
	{
		return "First Class";
	}
}
