package com.realdolmen.domain;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@DiscriminatorValue(value = "EMPLOYEE")
@Entity
public class Employee extends User {
}
