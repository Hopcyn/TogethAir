package com.realdolmen.domain;


import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
public class Booking implements Serializable {

	@Id
	@GeneratedValue
	private Long id;

	@Min(value =1,message = "{Lower_Than_Zero}")
	private int amountOfSeats;
	private BigDecimal totalPrice;
	private String travelingClass;
	private String status;
	private Boolean enabled;
	@OneToOne(fetch = FetchType.EAGER,targetEntity = User.class)
	private User user;
	@ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
	@JoinColumn(name = "flight_id")
	private Flight flight;
	@NotNull
	private String paymentMethodType;

	@Transient
	private PaymentMethod paymentMethod;

	public Booking() {
	}

	public Booking(int amountOfSeats, BigDecimal totalPrice, String travelingClass, String status, Boolean enabled, User user, Flight flight, @NotNull String paymentMethodType, PaymentMethod paymentMethod) {
		this.amountOfSeats = amountOfSeats;
		this.totalPrice = totalPrice;
		this.travelingClass = travelingClass;
		this.status = status;
		this.enabled = enabled;
		this.user = user;
		this.flight = flight;
		this.paymentMethodType = paymentMethodType;
		this.paymentMethod = paymentMethod;
	}

	public String getPaymentMethodType() {
		return paymentMethodType;
	}

	public void setPaymentMethodType(String paymentMethodType) {
		this.paymentMethodType = paymentMethodType;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public int getAmountOfSeats() {
		return amountOfSeats;
	}

	public void setAmountOfSeats(int amountOfSeats) {
		this.amountOfSeats = amountOfSeats;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getTravelingClass() {
		return travelingClass;
	}

	public void setTravelingClass(String travelingClass) {
		this.travelingClass = travelingClass;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
