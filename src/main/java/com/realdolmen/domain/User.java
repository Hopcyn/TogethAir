package com.realdolmen.domain;


import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;


@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "USER_ROLE")
@Entity
public abstract class User implements Serializable {
	@Id
	@GeneratedValue
	private Long id;

	@NotBlank(message = "{Field_Empty}")
	@Size(max = 200, message = "{Field_Size}")
	private String firstName;

	@NotBlank(message = "{Field_Empty}")
	@Size(max = 200, message = "{Field_Size}")
	private String lastName;

	@NotBlank(message = "{Field_Empty}")
	@Size(max = 254, message = "{Email_Size}")
	@Email(message = "{Email_Pattern}")
	private String emailAddress;

	@NotBlank(message = "{Password_Empty}")
	@Size(max = 128, message = "{Password_Size}")
	private String password;

	private Boolean enabled = true;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
}
