package com.realdolmen.domain;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@DiscriminatorValue(value = "CUSTOMER")
@Entity
public class Customer extends User {
}
