package com.realdolmen.domain;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@DiscriminatorValue(value = "ECONOMY_CLASS")
@Entity
public class EconomyClass  extends FlightClass{

	public String getClassName()
	{
		return "Economy Class";
	}
}
