package com.realdolmen.domain;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "FLIGHT_CLASS")
@Entity
public abstract class FlightClass implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@DecimalMin(value = "1", message = "{Lower_Than_One}")
	@NotNull(message = "{Field_Empty}")
	private BigDecimal basePrice = new BigDecimal(0);
	@NotNull(message = "{Field_Empty}")
	@Min(value = 1, message = "{Lower_Than_One}")
	private int amountOfSeats;
	@NotNull(message = "{Field_Empty}")
	@Min(value = 1, message = "{Lower_Than_One}")
	private int maxAmountOFSeats;
	@NotNull(message = "{Field_Empty}")
	private BigDecimal profitMargin;

	public BigDecimal getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public int getAmountOfSeats() {
		return amountOfSeats;
	}

	public void setAmountOfSeats(int amountOfSeats) {
		this.amountOfSeats = amountOfSeats;
	}

	public BigDecimal getProfitMargin() {
		return profitMargin;
	}

	public void setProfitMargin(BigDecimal profitMargin) {
		this.profitMargin = profitMargin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getMaxAmountOFSeats() {
		return maxAmountOFSeats;
	}

	public void setMaxAmountOFSeats(int maxAmountOFSeats) {
		this.maxAmountOFSeats = maxAmountOFSeats;
	}
}
