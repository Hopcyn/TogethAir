package com.realdolmen.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@DiscriminatorValue(value = "BUSINESS_CLASS")
@Entity
public class BusinessClass extends FlightClass {

	public String getClassName()
	{
		return "Business Class";
	}
}
