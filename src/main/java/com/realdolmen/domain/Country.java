package com.realdolmen.domain;

import com.realdolmen.enumeration.GlobalRegion;

import javax.persistence.*;

@Entity
public class Country {
	@Id
	@GeneratedValue
	private Long id;

	private String name;

	@Enumerated(EnumType.STRING)
	private GlobalRegion globalRegion;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GlobalRegion getGlobalRegion() {
		return globalRegion;
	}

	public void setGlobalRegion(GlobalRegion globalRegion) {
		this.globalRegion = globalRegion;
	}

	@Override
	public String toString() {
		return this.name + " (" + this.globalRegion + ")";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
