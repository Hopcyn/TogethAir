package com.realdolmen.domain;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
public class Flight implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull(message = "{Field_Not_Selected}")
	private LocalDateTime departureTime;
	private LocalTime duration;
	private Boolean enabled;
	@OneToMany(mappedBy = "flight",fetch = FetchType.EAGER)
	private List<Booking> bookingsList;
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "flight_id")
	private List<Discount> discountList = new ArrayList<Discount>();
	@OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@NotNull(message = "{Field_Not_Selected}")
	private Location departure;
	@NotNull(message = "{Field_Not_Selected}")
	@OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	private Location destination;
	@OneToOne
	private Partner partner;
	@OneToOne(fetch = FetchType.EAGER,targetEntity = FlightClass.class, cascade = CascadeType.ALL)
	private FirstClass firstClass = new FirstClass();
	@OneToOne(fetch = FetchType.EAGER,targetEntity = FlightClass.class, cascade = CascadeType.ALL)
	private BusinessClass businessClass = new BusinessClass();
	@OneToOne(fetch = FetchType.EAGER,targetEntity = FlightClass.class, cascade = CascadeType.ALL)
	private EconomyClass economyClass = new EconomyClass();

	@Transient
	@Future(message = "{Date_Future}")
	private Date departureTimeAsDate;

	@Transient
	private String durationTimeAsString;

	public FirstClass getFirstClass() {
		return firstClass;
	}

	public void setFirstClass(FirstClass firstClass) {
		this.firstClass = firstClass;
	}

	public BusinessClass getBusinessClass() {
		return businessClass;
	}

	public void setBusinessClass(BusinessClass businessClass) {
		this.businessClass = businessClass;
	}

	public EconomyClass getEconomyClass() {
		return economyClass;
	}

	public void setEconomyClass(EconomyClass economyClass) {
		this.economyClass = economyClass;
	}

	public LocalTime getDuration() {
		return duration;
	}

	public void setDuration(LocalTime duration) {
		this.duration = duration;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public List<Booking> getBookingsList() {
		return bookingsList;
	}

	public void setBookingsList(List<Booking> bookingsList) {
		this.bookingsList = bookingsList;
	}

	public List<Discount> getDiscountList() {
		return discountList;
	}

	public void setDiscountList(List<Discount> discountList) {
		this.discountList = discountList;
	}

	public Location getDeparture() {
		return departure;
	}

	public void setDeparture(Location departure) {
		this.departure = departure;
	}

	public Location getDestination() {
		return destination;
	}

	public void setDestination(Location destination) {
		this.destination = destination;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public LocalDateTime getDepartureTime() {
		return departureTime;
	}

	public String getDepartureTimeAsString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		return this.departureTime != null ? this.departureTime.toLocalDate().format(formatter) + " - " + this.departureTime.toLocalTime() : "";
	}

	public void setDepartureTime(LocalDateTime departureTime) {
		this.departureTime = departureTime;
	}

	public Date getDepartureTimeAsDate() {
		return this.departureTimeAsDate != null ? this.departureTimeAsDate : new Date();
	}

	public void setDepartureTimeAsDate(Date departureTimeAsDate) {
		this.departureTimeAsDate = departureTimeAsDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDurationTimeAsString() {
		return durationTimeAsString;
	}

	public void setDurationTimeAsString(String durationTimeAsString) {
		this.durationTimeAsString = durationTimeAsString;
	}
}
