package com.realdolmen.domain;

import java.time.LocalDate;

public class CreditCard extends PaymentMethod {
	private String number;
	private LocalDate expirationDate;

	public CreditCard() {
	}

	public CreditCard(String number, LocalDate expirationDate) {
		this.number = number;
		this.expirationDate = expirationDate;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public LocalDate getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDate expirationDate) {
		this.expirationDate = expirationDate;
	}
}
