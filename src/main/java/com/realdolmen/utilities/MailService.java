package com.realdolmen.utilities;

import com.realdolmen.domain.User;

import com.sendgrid.*;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by RCCBH67 on 16/09/2018.
 */
public class MailService implements Serializable{

    private final String SENDGRID_API_KEY = "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";
    private final String USER_REGISTRATION_TEMPLATE = "d-8950123da217467a9f09baf5604358be";
    private final String FLIGHT_BOOKING_TEMPLATE = "d-03bd8cf3c7fa4dbf98b689d842a67f05";


    public void confirmRegistration(User user) throws IOException {
        Email from = new Email("help@togethair.com");
        String subject = "Uw registratie bij TogethAir";
        Email to = new Email(user.getEmailAddress());

        /*Personalization personalization = new Personalization();
        personalization.addTo(to);
        personalization.addSubstitution("-firstName-", user.getFirstName());
        personalization.addSubstitution("-lastName-", user.getLastName());*/

        Content content = new Content("text/html", "text");

        Mail mail = new Mail(from, subject, to, content);
        mail.setTemplateId(this.USER_REGISTRATION_TEMPLATE);
        /*mail.addPersonalization(personalization);*/

        SendGrid sg = new SendGrid(this.SENDGRID_API_KEY);
        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        request.setBody(mail.build());
        Response response = sg.api(request);
    }

    public void confirmBooking(User user, byte[] bookingPdf) throws IOException {
        Email from = new Email("help@togethair.com");
        String subject = "Uw boeking bij TogethAir";
        Email to = new Email(user.getEmailAddress());

       /* Personalization personalization = new Personalization();
        personalization.addTo(to);
        personalization.addSubstitution("-firstName-", user.getFirstName());
        personalization.addSubstitution("-lastName-", user.getLastName());*/

        Content content = new Content("text/html", "text");

        Mail mail = new Mail(from, subject, to, content);
        mail.setTemplateId(this.FLIGHT_BOOKING_TEMPLATE);
       /* mail.addPersonalization(personalization);
*/
        Attachments attachments = new Attachments();
        Base64 base64Attachment = new Base64();
        String encodedString = base64Attachment.encodeAsString(bookingPdf);
        attachments.setContent(encodedString);
        attachments.setDisposition("attachment");
        attachments.setFilename("Booking_TogethAir.pdf");
        attachments.setType("application/pdf");
        mail.addAttachments(attachments);

        SendGrid sg = new SendGrid(this.SENDGRID_API_KEY);
        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        request.setBody(mail.build());
        Response response = sg.api(request);
    }
}
