package com.realdolmen.controller;


import com.realdolmen.domain.Flight;
import com.realdolmen.service.FlightService;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Named
public class FlightController {

	@Inject
	FlightService flightService;

	private Flight flight;

	@PostConstruct
	public void init()
	{
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String idAsString = req.getParameter("flightId");
		if(idAsString != null)
		{
			Long id = Long.parseLong(idAsString);
			flight = flightService.findById(id);
		}
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}
}
