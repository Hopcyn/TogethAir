package com.realdolmen.controller;


import com.realdolmen.domain.Booking;
import com.realdolmen.domain.Flight;
import com.realdolmen.domain.Location;
import com.realdolmen.scheduled.SaveAllBookings;
import com.realdolmen.service.FlightService;
import com.realdolmen.service.LocationService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ManagedBean
@RequestScoped
public class HomeController {

	@Inject
	LocationService locationService;

	@Inject
	FlightService flightService;

	@Inject
	BookingController bookingController;



	@NotNull(message = "{Field_Empty}")
	@Future(message = "{Date_Future}")
	private Date departureDate;

	@NotNull(message = "{Field_Empty}")
	private Location departure;

	@NotNull(message = "{Field_Empty}")
	private Location destination;


	private List<Location> locationList = new ArrayList<>();
	private List<Flight> foundFlights = new ArrayList<>();

	public void search()
	{
		foundFlights =	flightService.getFlightFromToWithDate(departure,destination,departureDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
	}

	@PostConstruct
	public void init()
	{

		locationList = locationService.getAllActiveLocations();
		bookingController.setDiscountPercentage(0);
		bookingController.setBooking(new Booking());
		bookingController.setFlight(new Flight());
	}

	public List<Location> completeLocation(String query) {
		List<Location> filteredLocations = new ArrayList<Location>();

		for (int i = 0; i < locationList.size(); i++) {
			Location skin = locationList.get(i);
			if(skin.getAirportName().toLowerCase().startsWith(query.toLowerCase())) {
				filteredLocations.add(skin);
			}
		}

		return filteredLocations;
	}


	public List<Location> getLocationList() {
		return locationList;
	}

	public void setLocationList(List<Location> locationList) {
		this.locationList = locationList;
	}

	public Location getDeparture() {
		return departure;
	}

	public void setDeparture(Location departure) {
		this.departure = departure;
	}

	public Location getDestination() {
		return destination;
	}

	public void setDestination(Location destination) {
		this.destination = destination;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public List<Flight> getFoundFlights() {
		return foundFlights;
	}

	public void setFoundFlights(List<Flight> foundFlights) {
		this.foundFlights = foundFlights;
	}
}
