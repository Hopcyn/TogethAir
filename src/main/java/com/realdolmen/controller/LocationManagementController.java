package com.realdolmen.controller;

import com.realdolmen.bean.DateTimeBean;
import com.realdolmen.bean.GrowlBean;
import com.realdolmen.bean.UserBean;
import com.realdolmen.converter.DateConverter;
import com.realdolmen.domain.*;
import com.realdolmen.dto.UserDTO;
import com.realdolmen.service.CountryService;
import com.realdolmen.service.DiscountService;
import com.realdolmen.service.FlightService;
import com.realdolmen.service.LocationService;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RCCBH67 on 12/09/2018.
 */
@Named
@ViewScoped
public class LocationManagementController implements Serializable{
    @Inject
    private LocationService locationService;

    @Inject
    private CountryService countryService;

    private List<Location> locationList = new ArrayList<Location>();

    private Location locationToSave = new Location();

    private List<Country> countryList = new ArrayList<Country>();

    @PostConstruct
    public void init()
    {
        this.locationToSave = new Location();
        this.locationList = this.locationService.getAllLocations();
        this.countryList = this.countryService.getAllCountries();
    }

    public List<Location> getLocationList() {
        return locationList;
    }

    public void saveNewLocation(){
        this.locationToSave.setEnabled(true);
        this.locationService.merge(this.locationToSave);
        this.init();
    }

    public void disableLocation(Location location){
        location.setEnabled(false);
        this.locationService.merge(location);
    }

    public Location getLocationToSave() {
        return locationToSave;
    }

    public List<Country> getCountryList() {
        return countryList;
    }
}
