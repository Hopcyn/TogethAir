package com.realdolmen.controller;

import com.realdolmen.domain.Customer;
import com.realdolmen.domain.Flight;
import com.realdolmen.service.CustomerService;
import com.realdolmen.service.FlightService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
public class CustomerController {

	@Inject
	CustomerService customerService;

	private List<Customer> customerList = new ArrayList<>();
	private Customer customer;

	@PostConstruct
	public void init()
	{
		customerList = customerService.getAllCustomers();
	}


	public List<Customer> completeTheme(String query) {
		List<Customer> allThemes = customerList;
		List<Customer> filteredThemes = new ArrayList<Customer>();

		for (int i = 0; i < allThemes.size(); i++) {
			Customer skin = allThemes.get(i);
			if(skin.getFirstName().toLowerCase().startsWith(query)) {
				filteredThemes.add(skin);
			}
		}

		return filteredThemes;
	}

	public List<Customer> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<Customer> customerList) {
		this.customerList = customerList;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}
