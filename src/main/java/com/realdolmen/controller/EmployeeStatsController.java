package com.realdolmen.controller;


import com.realdolmen.bean.UserBean;
import com.realdolmen.domain.Booking;
import com.realdolmen.domain.Employee;
import com.realdolmen.domain.Flight;
import com.realdolmen.domain.Partner;
import com.realdolmen.service.BookingService;
import com.realdolmen.service.FlightService;
import org.primefaces.model.chart.*;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Named
@RequestScoped
public class EmployeeStatsController {

	@Inject
	FlightService flightService;

	@Inject
	UserBean userBean;

	@Inject
	BookingService bookingService;

	private List<Flight> listOfAllFlights = new ArrayList<>();
	private List<Booking> listOfAllBookingsConfirmed = new ArrayList<>();
	private List<Booking> listOfAllBookingsPending = new ArrayList<>();
	private List<Booking> listOfAllBookingsFromPartner = new ArrayList<>();
	private int numberOfFlights;
	private int numberOfBookingsPending;
	private int numberOfBookingsConfirmed;
	private int numberOfBookings;
	private HorizontalBarChartModel bookingStatusChart;
	private LineChartModel bookingPricesChart;
	private BigDecimal highestPrice = new BigDecimal(0);
	private Double averagePriceAllBookings;
	private BigDecimal maxPriceAllBookings;
	private BigDecimal minPriceAllBookings;

	@PostConstruct
	public void init() {
		setup();
		bookingStatusChartInit();
		bookingPricesChartInit();

	}

	private void setup() {
		listOfAllFlights = flightService.getAllFlights();
		numberOfFlights = listOfAllFlights.size();
		listOfAllBookingsConfirmed = bookingService.getAllBookingsByStatus("CONFIRMED");
		numberOfBookingsConfirmed = listOfAllBookingsConfirmed.size();
		listOfAllBookingsPending = bookingService.getAllBookingsByStatus("PENDING");
		numberOfBookingsPending = listOfAllBookingsPending.size();
		listOfAllBookingsFromPartner = bookingService.getAllBookings();

		averagePriceAllBookings = bookingService.getAvgPriceCostFromAllBookings();
		minPriceAllBookings = bookingService.getMinPriceCostFromAllBookings();
		maxPriceAllBookings = bookingService.getMaxPriceCostFromAllBookings();
	}


	private void bookingStatusChartInit() {
		bookingStatusChart = new HorizontalBarChartModel();

		ChartSeries confirmed = new ChartSeries();
		confirmed.setLabel("Confirmed");
		confirmed.set("Flights", numberOfBookingsConfirmed);


		ChartSeries pending = new ChartSeries();
		pending.setLabel("Pending");
		pending.set("Flights", numberOfBookingsPending);


		bookingStatusChart.addSeries(confirmed);
		bookingStatusChart.addSeries(pending);

		bookingStatusChart.setTitle("Flights and their status");
		bookingStatusChart.setLegendPosition("e");

		Axis xAxis = bookingStatusChart.getAxis(AxisType.X);
		xAxis.setMin(0);
		xAxis.setTickCount(5);
		xAxis.setMax(numberOfBookingsConfirmed + numberOfBookingsPending + 10);
	}

	private void bookingPricesChartInit() {
		bookingPricesChart = initPriceModel();

		bookingPricesChart.setTitle("Prices Chart - average price is " + averagePriceAllBookings
				+ " min price is " +minPriceAllBookings
				+ " max price is "+maxPriceAllBookings
		);
		bookingPricesChart.setLegendPosition("e");
		bookingPricesChart.setShowPointLabels(true);
		Axis yAxis = bookingPricesChart.getAxis(AxisType.Y);
		yAxis.setMin(0);
		yAxis.setMax(highestPrice.add(new BigDecimal(10)));

		Axis xAxis = bookingPricesChart.getAxis(AxisType.X);
		xAxis.setMin(0);
		xAxis.setMax(listOfAllBookingsFromPartner.size());
		xAxis.setTickCount(5);
	}

	private LineChartModel initPriceModel() {
		LineChartModel model = new LineChartModel();

		ChartSeries prices = new ChartSeries();
		prices.setLabel("Prices");

		int teller = 0;
		for (Booking b : listOfAllBookingsFromPartner) {
			prices.set(++teller, b.getTotalPrice());
			if (b.getTotalPrice().compareTo(highestPrice) > 0) {
				highestPrice = b.getTotalPrice();
			}
		}

		model.addSeries(prices);

		return model;
	}


	public List<Flight> getListOfAllFlights() {
		return listOfAllFlights;
	}

	public void setListOfAllFlights(List<Flight> listOfAllFlights) {
		this.listOfAllFlights = listOfAllFlights;
	}

	public int getNumberOfFlights() {
		return numberOfFlights;
	}

	public void setNumberOfFlights(int numberOfFlights) {
		this.numberOfFlights = numberOfFlights;
	}

	public List<Booking> getListOfAllBookingsConfirmed() {
		return listOfAllBookingsConfirmed;
	}

	public void setListOfAllBookingsConfirmed(List<Booking> listOfAllBookingsConfirmed) {
		this.listOfAllBookingsConfirmed = listOfAllBookingsConfirmed;
	}

	public List<Booking> getListOfAllBookingsPending() {
		return listOfAllBookingsPending;
	}

	public void setListOfAllBookingsPending(List<Booking> listOfAllBookingsPending) {
		this.listOfAllBookingsPending = listOfAllBookingsPending;
	}

	public int getNumberOfBookingsPending() {
		return numberOfBookingsPending;
	}

	public void setNumberOfBookingsPending(int numberOfBookingsPending) {
		this.numberOfBookingsPending = numberOfBookingsPending;
	}

	public int getNumberOfBookingsConfirmed() {
		return numberOfBookingsConfirmed;
	}

	public void setNumberOfBookingsConfirmed(int numberOfBookingsConfirmed) {
		this.numberOfBookingsConfirmed = numberOfBookingsConfirmed;
	}

	public HorizontalBarChartModel getBookingStatusChart() {
		return bookingStatusChart;
	}

	public void setBookingStatusChart(HorizontalBarChartModel bookingStatusChart) {
		this.bookingStatusChart = bookingStatusChart;
	}

	public LineChartModel getBookingPricesChart() {
		return bookingPricesChart;
	}

	public void setBookingPricesChart(LineChartModel bookingPricesChart) {
		this.bookingPricesChart = bookingPricesChart;
	}

	public int getNumberOfBookings() {
		return numberOfBookings;
	}

	public void setNumberOfBookings(int numberOfBookings) {
		this.numberOfBookings = numberOfBookings;
	}

	public List<Booking> getListOfAllBookingsFromPartner() {
		return listOfAllBookingsFromPartner;
	}

	public void setListOfAllBookingsFromPartner(List<Booking> listOfAllBookingsFromPartner) {
		this.listOfAllBookingsFromPartner = listOfAllBookingsFromPartner;
	}

	public BigDecimal getHighestPrice() {
		return highestPrice;
	}

	public void setHighestPrice(BigDecimal highestPrice) {
		this.highestPrice = highestPrice;
	}

	public Double getAveragePriceAllBookings() {
		return averagePriceAllBookings;
	}

	public void setAveragePriceAllBookings(Double averagePriceAllBookings) {
		this.averagePriceAllBookings = averagePriceAllBookings;
	}

	public BigDecimal getMaxPriceAllBookings() {
		return maxPriceAllBookings;
	}

	public void setMaxPriceAllBookings(BigDecimal maxPriceAllBookings) {
		this.maxPriceAllBookings = maxPriceAllBookings;
	}

	public BigDecimal getMinPriceAllBookings() {
		return minPriceAllBookings;
	}

	public void setMinPriceAllBookings(BigDecimal minPriceAllBookings) {
		this.minPriceAllBookings = minPriceAllBookings;
	}
}
