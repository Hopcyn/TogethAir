package com.realdolmen.controller;

import com.realdolmen.bean.NavigationBean;
import com.realdolmen.bean.UserBean;
import com.realdolmen.domain.*;
import com.realdolmen.generator.ReportGenerator;
import com.realdolmen.service.BookingService;
import com.realdolmen.service.FlightService;
import com.realdolmen.utilities.MailService;
import net.sf.jasperreports.engine.*;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static java.lang.System.in;

@Named
@SessionScoped
public class BookingController implements Serializable {

	@Inject
	FlightService flightService;

	@Inject
	UserBean userBean;

	@Inject
	BookingService bookingService;

	@Inject
	private NavigationBean navigationBean;

	@Inject
	private MailService mailService;

	private Flight flight = new Flight();
	private String flightClass;
	private Booking booking;
	@NotNull(message = "{Empty}")
	private String paymentMethod;
	private Date creditCardExpDate;
	private String creditCardNumber;
	private int discountPercentage;
	private FlightClass currentFlightClass;
	private int currentDiscountAmountOfSeats;
	private ReportGenerator reportGenerator =  new ReportGenerator();

	@PostConstruct
	public void init() {
		booking = new Booking();

	}

	public void onLoad()
	{
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

		if(flight.getId() == null) {

			currentDiscountAmountOfSeats = 0;

			String idAsString = req.getParameter("flightId");
			flightClass = req.getParameter("flightClass");
			booking.setTravelingClass(flightClass);

			if (idAsString != null || !booking.getStatus().equals("CONFIRMED")) {
				Long id = Long.parseLong(idAsString);
				flight = flightService.findById(id);
				setupFlightClass();
			}
		}
	}

	private void setupFlightClass()
	{
		switch (flightClass) {
			case "firstClass":
				currentFlightClass = flight.getFirstClass();
				break;
			case "economyClass":
				currentFlightClass = flight.getEconomyClass();
				break;
			case "businessClass":
				currentFlightClass = flight.getBusinessClass();
				break;
		}
	}


	public String pay() {
		setupDiscount();
		setupBooking();
		return navigationBean.navigateToConfirmBooking(true);
	}

	private void setupDiscount() {
		List<Discount> discountList = flight.getDiscountList();
		 currentDiscountAmountOfSeats = 0;

		for (Discount discount : discountList) {
			if (booking.getAmountOfSeats() >= discount.getAmountOfSeats()) {
				//booking can get a discount because has more seats than the discount requires
				if (discount.getAmountOfSeats() > currentDiscountAmountOfSeats) {
					//current discount seat count is higher than the previous applied discount so this one counts
					currentDiscountAmountOfSeats = discount.getAmountOfSeats();
					discountPercentage = discount.getPercentage();
				}
			}
		}

	}

	private void setupBooking() {
		if (paymentMethod.equals("CreditCard")) {
			LocalDate creditCardExpire = creditCardExpDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			CreditCard creditCard = new CreditCard(creditCardNumber, creditCardExpire);
			booking.setPaymentMethod(creditCard);
		} else {
			booking.setPaymentMethod(new Endorsement());
		}


		switch (flightClass) {
			case "firstClass":
				booking.setTotalPrice
						((
								flight.getFirstClass().getBasePrice()
										.add(flight.getFirstClass().getProfitMargin()))
								.multiply(new BigDecimal(booking.getAmountOfSeats()))
						);
				flight.getFirstClass().setAmountOfSeats(flight.getFirstClass().getAmountOfSeats() - booking.getAmountOfSeats());
				break;
			case "economyClass":
				booking.setTotalPrice
						((
								flight.getEconomyClass().getBasePrice()
										.add(flight.getEconomyClass().getProfitMargin()))
								.multiply(new BigDecimal(booking.getAmountOfSeats()))
						);
				flight.getEconomyClass().setAmountOfSeats(flight.getEconomyClass().getAmountOfSeats() - booking.getAmountOfSeats());
				break;
			case "businessClass":
				booking.setTotalPrice
						((
								flight.getBusinessClass().getBasePrice()
										.add(flight.getBusinessClass().getProfitMargin()))
								.multiply(new BigDecimal(booking.getAmountOfSeats()))
						);
				flight.getBusinessClass().setAmountOfSeats(flight.getBusinessClass().getAmountOfSeats() - booking.getAmountOfSeats());
				break;
		}

		//apply discount
		booking.setTotalPrice(calculateTotalPriceWithDiscount());

		booking.setFlight(flight);
		booking.setStatus("TO_PAY");
		booking.setEnabled(true);
		booking.setUser(userBean.getUser());

	}

	public String confirmBooking() {

		if(booking.getPaymentMethodType().equals("CreditCard"))
		{
			booking.setStatus("CONFIRMED");
		}
		else
		{
			booking.setStatus("PENDING");
		}
		bookingService.save(booking);
		flightService.merge(flight);

		byte[] pdf = reportGenerator.generate(booking,discountPercentage);

		try {
			this.mailService.confirmBooking(this.userBean.getUser(), pdf);
		} catch (IOException e) {
			e.printStackTrace();
		}

		flight = new Flight();
		booking = new Booking();

		FacesMessage message = new FacesMessage("Vlucht geboekt", "U heeft de vlucht succesvol geboekt.");
		FacesContext.getCurrentInstance().addMessage(null, message);
		return navigationBean.navigateToBookingSuccess(false);
	}

	private BigDecimal calculateTotalPriceWithDiscount() {
		if (discountPercentage == 0) {
			return booking.getTotalPrice();
		}
		BigDecimal discount = booking.getTotalPrice().divide(new BigDecimal(discountPercentage), 2);
		return booking.getTotalPrice().subtract(discount);
	}

	public String goBackToDetail() {
		return navigationBean.navigateToBookingDetail(true);

	}
	public String goBackToIndex()
	{
		flight = new Flight();
		booking = new Booking();
		return navigationBean.navigateToHome(true);
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public String getFlightClass() {
		return flightClass;
	}

	public void setFlightClass(String flightClass) {
		this.flightClass = flightClass;
	}

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}


	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		if (paymentMethod != null) {
			booking.setPaymentMethodType(paymentMethod);
		}
		this.paymentMethod = paymentMethod;
	}

	public Date getCreditCardExpDate() {
		return creditCardExpDate;
	}

	public void setCreditCardExpDate(Date creditCardExpDate) {
		this.creditCardExpDate = creditCardExpDate;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public int getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(int discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public FlightClass getCurrentFlightClass() {
		return currentFlightClass;
	}

	public void setCurrentFlightClass(FlightClass currentFlightClass) {
		this.currentFlightClass = currentFlightClass;
	}
}
