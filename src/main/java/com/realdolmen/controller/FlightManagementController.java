package com.realdolmen.controller;

import com.realdolmen.bean.DateTimeBean;
import com.realdolmen.bean.GrowlBean;
import com.realdolmen.bean.UserBean;
import com.realdolmen.converter.DateConverter;
import com.realdolmen.domain.*;
import com.realdolmen.dto.UserDTO;
import com.realdolmen.enumeration.GlobalRegion;
import com.realdolmen.service.DiscountService;
import com.realdolmen.service.FlightService;
import com.realdolmen.service.LocationService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.UploadedFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RCCBH67 on 12/09/2018.
 */
@Named
@ViewScoped
public class FlightManagementController implements Serializable {
	@Inject
	private FlightService flightService;

	@Inject
	private UserBean userBean;

	@Inject
	private LocationService locationService;

	@Inject
	private DiscountService discountService;

	@Inject
	private GrowlBean growlBean;

	@Inject
	private DateTimeBean dateTimeBean;

	@Inject
	private DateConverter dateConverter;


	private User user = new UserDTO();

	private Flight selectedFlight = new Flight();

	private List<Flight> flightList = new ArrayList<Flight>();

	private List<Location> locationList = new ArrayList<Location>();

	private Flight flightToSave = new Flight();

	private Discount discountToSave = new Discount();

	private UploadedFile file;

	@PostConstruct
	public void init() {
		this.discountToSave = new Discount();
		this.flightToSave = new Flight();
		this.flightList = this.flightService.getFlightsByPartner((Partner) this.userBean.getUser());
		this.locationList = this.locationService.getAllActiveLocations();
	}

	public List<Flight> getFlightList() {
		return flightList;
	}

	public Flight getFlightToSave() {
		return flightToSave;
	}

	public void setFlightToSave(Flight flightToSave) {
		this.flightToSave = flightToSave;
	}

	public List<Location> getLocationList() {
		return locationList;
	}

	public void saveNewFlight() {
		this.setFlightObject();
		this.flightService.merge(this.flightToSave);
		this.init();
	}

	private void setFlightObject() {
		this.flightToSave.setDepartureTime(this.dateConverter.convertDateToLocalDateTime(this.flightToSave.getDepartureTimeAsDate()));
		this.flightToSave.setPartner((Partner) this.userBean.getUser());
		this.flightToSave.setDuration(LocalTime.parse(this.flightToSave.getDurationTimeAsString() + ":00"));
		this.setFlightClassObjects();
		this.flightToSave.setEnabled(true);
	}

	private void setFlightClassObjects() {
		this.flightToSave.getFirstClass().setAmountOfSeats(this.flightToSave.getFirstClass().getMaxAmountOFSeats());
		this.flightToSave.getFirstClass().setProfitMargin(this.flightToSave.getFirstClass().getBasePrice().multiply(new BigDecimal(0.05)));

		this.flightToSave.getBusinessClass().setAmountOfSeats(this.flightToSave.getBusinessClass().getMaxAmountOFSeats());
		this.flightToSave.getBusinessClass().setProfitMargin(this.flightToSave.getBusinessClass().getBasePrice().multiply(new BigDecimal(0.05)));

		this.flightToSave.getEconomyClass().setAmountOfSeats(this.flightToSave.getEconomyClass().getMaxAmountOFSeats());
		this.flightToSave.getEconomyClass().setProfitMargin(this.flightToSave.getEconomyClass().getBasePrice().multiply(new BigDecimal(0.05)));
	}

	public void disableFlight(Flight flight) {
		flight.setDurationTimeAsString("Update");
		flight.setEnabled(false);
		this.flightService.merge(flight);
	}


	public void upload() {
		if (file != null) {
			FacesMessage message = new FacesMessage("File geüpload ", file.getFileName() + " is succesvol geüpload.");
			FacesContext.getCurrentInstance().addMessage(null, message);

			File uploadedFile = new File("uploadedFile");
			try {
				FileUtils.writeByteArrayToFile(uploadedFile, getContents(file));
			} catch (IOException e) {
				e.printStackTrace();
			}

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = null;
			try {
				dBuilder = dbFactory.newDocumentBuilder();

				Document doc = dBuilder.parse(uploadedFile);

				NodeList nList = doc.getElementsByTagName("flight");
				for (int temp = 0; temp < nList.getLength(); temp++) {

					Node nNode = nList.item(temp);

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						createFlight(nNode);
					}
				}

			} catch (ParserConfigurationException | SAXException | IOException e) {
				e.printStackTrace();
			}

		}
	}

	private void createFlight(Node nNode) {

		Flight f = new Flight();
		Location departure = new Location();
		Location destination = new Location();
		Country countryDeparture = new Country();
		Country countryDestination = new Country();
		FirstClass firstClass = new FirstClass();
		BusinessClass businessClass = new BusinessClass();
		EconomyClass economyClass = new EconomyClass();
		Element eElement = (Element) nNode;


		f.setPartner((Partner) userBean.getUser());

		f.setEnabled(Boolean.valueOf(eElement.getElementsByTagName("enabled").item(0).getTextContent()));
		f.setDepartureTime(LocalDateTime.parse(eElement.getElementsByTagName("departureTime").item(0).getTextContent()));
		f.setDuration(LocalTime.parse(eElement.getElementsByTagName("duration").item(0).getTextContent()));

		//Departure
		departure.setAirportName(eElement.getElementsByTagName("airportName").item(0).getTextContent());
		departure.setAirportCode(eElement.getElementsByTagName("airportCode").item(0).getTextContent());
		departure.setEnabled(Boolean.valueOf(eElement.getElementsByTagName("locationEnabled").item(0).getTextContent()));
		countryDeparture.setName(eElement.getElementsByTagName("countryName").item(0).getTextContent());
		countryDeparture.setGlobalRegion(GlobalRegion.valueOf(eElement.getElementsByTagName("globalRegion").item(0).getTextContent()));
		departure.setCountry(countryDeparture);

		Location possibleDeparture = locationService.getLocationByCode(departure.getAirportCode());
		if (possibleDeparture == null) {
			f.setDeparture(departure);
		} else {
			f.setDeparture(possibleDeparture);
		}

		//Destination
		destination.setAirportName(eElement.getElementsByTagName("airportNameDestination").item(0).getTextContent());
		destination.setAirportCode(eElement.getElementsByTagName("airportCodeDestination").item(0).getTextContent());
		destination.setEnabled(Boolean.valueOf(eElement.getElementsByTagName("locationEnabledDestination").item(0).getTextContent()));
		countryDestination.setName(eElement.getElementsByTagName("countryNameDestination").item(0).getTextContent());
		countryDestination.setGlobalRegion(GlobalRegion.valueOf(eElement.getElementsByTagName("globalRegionDestination").item(0).getTextContent()));
		destination.setCountry(countryDestination);

		Location possibleDestination = locationService.getLocationByCode(destination.getAirportCode());
		if (possibleDestination == null) {
			f.setDestination(destination);
		} else {
			f.setDestination(possibleDestination);
		}


		//Classes
		firstClass.setAmountOfSeats(Integer.parseInt(eElement.getElementsByTagName("firstClassAmountOfSeats").item(0).getTextContent()));
		firstClass.setMaxAmountOFSeats(Integer.parseInt(eElement.getElementsByTagName("firstClassMaxAmountOfSeats").item(0).getTextContent()));
		firstClass.setBasePrice(new BigDecimal(eElement.getElementsByTagName("firstClassBasePrice").item(0).getTextContent()));
		firstClass.setProfitMargin(firstClass.getBasePrice().divide(new BigDecimal(100).setScale(2, RoundingMode.HALF_UP)).multiply(new BigDecimal(5)));

		economyClass.setAmountOfSeats(Integer.parseInt(eElement.getElementsByTagName("economyClassAmountOfSeats").item(0).getTextContent()));
		economyClass.setMaxAmountOFSeats(Integer.parseInt(eElement.getElementsByTagName("economyClassMaxAmountOfSeats").item(0).getTextContent()));
		economyClass.setBasePrice(new BigDecimal(eElement.getElementsByTagName("economyClassBasePrice").item(0).getTextContent()));
		economyClass.setProfitMargin(economyClass.getBasePrice().divide(new BigDecimal(100).setScale(2, RoundingMode.HALF_UP)).multiply(new BigDecimal(5)));

		businessClass.setAmountOfSeats(Integer.parseInt(eElement.getElementsByTagName("businessClassAmountOfSeats").item(0).getTextContent()));
		businessClass.setMaxAmountOFSeats(Integer.parseInt(eElement.getElementsByTagName("businessClassMaxAmountOfSeats").item(0).getTextContent()));
		businessClass.setBasePrice(new BigDecimal(eElement.getElementsByTagName("businessClassBasePrice").item(0).getTextContent()));
		businessClass.setProfitMargin(businessClass.getBasePrice().divide(new BigDecimal(100).setScale(2, RoundingMode.HALF_UP)).multiply(new BigDecimal(5)));

		f.setFirstClass(firstClass);
		f.setEconomyClass(economyClass);
		f.setBusinessClass(businessClass);

		flightService.merge(f);
	}

	private byte[] getContents(UploadedFile uploadedFile) {

		try {
			if (uploadedFile.getContents() != null && uploadedFile.getContents().length > 0) {
				return uploadedFile.getContents();
			}

			if (uploadedFile.getSize() > 0) {

				byte[] bytes = IOUtils.toByteArray(uploadedFile.getInputstream());

				if (bytes != null && bytes.length > 0) {
					return bytes;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public User getUser() {
		return user;
	}

	public Flight getSelectedFlight() {
		return selectedFlight;
	}

	public void setSelectedFlight(Flight flight) {
		this.selectedFlight = flight;
	}

	public void saveNewDiscount() {
		this.discountToSave.setFlight(this.selectedFlight);
		this.discountService.save(this.discountToSave);
		this.selectedFlight = this.flightService.findById(this.selectedFlight.getId());
		this.init();
	}

	public Discount getDiscountToSave() {
		return discountToSave;
	}

	public void setDiscountToSave(Discount discountToSave) {
		this.discountToSave = discountToSave;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}
}
