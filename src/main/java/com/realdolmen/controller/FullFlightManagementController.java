package com.realdolmen.controller;

import com.realdolmen.bean.UserBean;
import com.realdolmen.domain.*;
import com.realdolmen.enumeration.GlobalRegion;
import com.realdolmen.service.FlightService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.UploadedFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RCCBH67 on 12/09/2018.
 */
@Named
@ViewScoped
public class FullFlightManagementController implements Serializable {
	@Inject
	private FlightService flightService;

	@Inject
	private UserBean userBean;

	private Flight selectedFlight = new Flight();

	private List<Flight> flightList = new ArrayList<Flight>();


	@PostConstruct
	public void init() {
		this.flightList = this.flightService.getAllFlights();
	}

	public Flight getSelectedFlight() {
		return selectedFlight;
	}

	public void setSelectedFlight(Flight flight) {
		this.selectedFlight = flight;
	}

	public void saveFlight() {
		this.flightService.merge(this.selectedFlight);
		this.flightList = this.flightService.getAllFlights();
		this.init();
	}



	public List<Flight> getFlightList() {
		return flightList;
	}

}
