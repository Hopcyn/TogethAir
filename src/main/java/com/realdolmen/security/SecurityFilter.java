package com.realdolmen.security;

import com.realdolmen.bean.NavigationBean;
import com.realdolmen.bean.UserBean;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by RCCBH67 on 12/09/2018.
 */
public class SecurityFilter implements Filter {
    @Inject
    private UserBean userBean;

    @Inject
    private NavigationBean navigationBean;

    private FilterConfig fc;


    public void init(FilterConfig filterConfig)throws ServletException {
        this.fc = filterConfig;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException{
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession(true);

        String pageRequested = req.getRequestURI().toString();

        if(pageRequested.contains("login.xhtml") && userBean.isLoggedIn()){
            resp.sendRedirect(this.navigationBean.navigateToHome(true));
        }
        else if(pageRequested.contains("register.xhtml") && userBean.isLoggedIn()){
            resp.sendRedirect(this.navigationBean.navigateToHome(true));
        }
        else if(pageRequested.contains("flightOverview.xhtml") && !userBean.isPartner()){
            resp.sendRedirect(this.navigationBean.navigateTo404Page());
        }
        else if(pageRequested.contains("booking.xhtml") && !userBean.isLoggedIn()){
            resp.sendRedirect(this.navigationBean.navigateToLogin(false));
        }
        else if(pageRequested.contains("fullFlightOverview.xhtml") && !userBean.isEmployee()){
            resp.sendRedirect(this.navigationBean.navigateTo404Page());
        }
        else if(pageRequested.contains("locationOverview.xhtml") && !userBean.isEmployee()){
            resp.sendRedirect(this.navigationBean.navigateTo404Page());
        }
        else{
            chain.doFilter(request, response);
        }
    }

    public void destroy(){

    }
}
