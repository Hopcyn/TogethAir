package com.realdolmen.service;

import com.realdolmen.domain.Booking;
import com.realdolmen.domain.Country;
import com.realdolmen.remoteBean.BookingServiceRemote;
import com.realdolmen.remoteBean.CountryServiceRemote;
import com.realdolmen.repository.BookingRepository;
import com.realdolmen.repository.CountryRepository;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.List;

@Stateless
@LocalBean
public class CountryService implements CountryServiceRemote, Serializable {

	@EJB
	CountryRepository countryRepository;

	@Override
	public List<Country> getAllCountries() {
		return countryRepository.getAllCountries();
	}
}
