package com.realdolmen.service;

import com.realdolmen.domain.Customer;
import com.realdolmen.domain.User;
import com.realdolmen.remoteBean.CustomerServiceRemote;
import com.realdolmen.repository.CustomerRepository;

import javax.ejb.EJB;
import java.io.Serializable;
import java.util.List;

public class CustomerService implements CustomerServiceRemote, Serializable {

	@EJB
	CustomerRepository customerRepository;

	@Override
	public List<Customer> getAllCustomers() {
		return customerRepository.getAllCustomers();
	}

}
