package com.realdolmen.service;

import com.realdolmen.domain.Customer;
import com.realdolmen.domain.User;
import com.realdolmen.remoteBean.CustomerServiceRemote;
import com.realdolmen.remoteBean.UserServiceRemote;
import com.realdolmen.repository.CustomerRepository;
import com.realdolmen.repository.UserRepository;

import javax.ejb.EJB;
import java.io.Serializable;
import java.util.List;

public class UserService implements UserServiceRemote, Serializable {

	@EJB
	UserRepository userRepository;


	@Override
	public List<User> getAllUsers() {
		return this.userRepository.getAllUsers();
	}

	@Override
	public User getUserByCredentials(User user) {
		return this.userRepository.getUserByCredentials(user);
	}

	@Override
	public void persist(User user) {
		userRepository.persist(user);
	}
}
