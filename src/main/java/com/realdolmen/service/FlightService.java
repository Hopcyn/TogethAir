package com.realdolmen.service;

import com.realdolmen.domain.Flight;
import com.realdolmen.domain.Location;
import com.realdolmen.domain.Partner;
import com.realdolmen.remoteBean.FlightServiceRemote;
import com.realdolmen.repository.FlightRepository;
import org.jboss.weld.context.ejb.Ejb;

import javax.ejb.EJB;
import javax.inject.Inject;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class FlightService implements FlightServiceRemote, Serializable {

	@EJB
	FlightRepository flightRepository;

	@Override
	public List<Flight> getAllFlights() {
		return this.flightRepository.getAllFlights();
	}

	@Override
	public List<Flight> getFlightFromToWithDate(Location departure, Location destination, LocalDateTime date) {
		return this.flightRepository.getFlightFromToWithDate(departure,destination,date);
	}

	@Override
	public List<Flight> getFlightsByPartner(Partner partner) {
		return this.flightRepository.getFlightsByPartnerId(partner);
	}


	@Override
	public Flight findById(Long id) {
		return this.flightRepository.findById(id);
	}

	@Override
	public void merge(Flight flight) {
		this.flightRepository.merge(flight);
	}

	@Override
	public void persist(Flight flight) {
		this.flightRepository.persist(flight);
	}
}
