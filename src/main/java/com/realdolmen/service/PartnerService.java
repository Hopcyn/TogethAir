package com.realdolmen.service;

import com.realdolmen.domain.Partner;
import com.realdolmen.remoteBean.PartnerServiceRemote;
import com.realdolmen.repository.PartnerRepository;

import javax.ejb.EJB;
import java.io.Serializable;
import java.util.List;


public class PartnerService implements PartnerServiceRemote, Serializable {

	@EJB
	PartnerRepository partnerRepository;

	@Override
	public List<Partner> getAllPartners() {
		return partnerRepository.getAllPartners();
	}
}
