package com.realdolmen.service;

import com.realdolmen.domain.Booking;
import com.realdolmen.domain.Discount;
import com.realdolmen.remoteBean.BookingServiceRemote;
import com.realdolmen.remoteBean.DiscountServiceRemote;
import com.realdolmen.repository.BookingRepository;
import com.realdolmen.repository.DiscountRepository;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.List;

@Stateless
@LocalBean
public class DiscountService implements DiscountServiceRemote, Serializable {

	@EJB
	DiscountRepository discountRepository;


	@Override
	public void save(Discount discount) {
		this.discountRepository.save(discount);
	}
}
