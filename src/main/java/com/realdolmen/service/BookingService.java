package com.realdolmen.service;

import com.realdolmen.domain.Booking;
import com.realdolmen.remoteBean.BookingServiceRemote;
import com.realdolmen.repository.BookingRepository;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Stateless
@LocalBean
public class BookingService implements BookingServiceRemote, Serializable {

	@EJB
	BookingRepository bookingRepository;

	@Override
	public List<Booking> getAllBookings() {
		return bookingRepository.getAllBookings();
	}

	@Override
	public void save(Booking booking) {
		bookingRepository.save(booking);
	}

	@Override
	public List<Booking> getAllBookingsByStatus(String status) {
		return bookingRepository.getAllBookingsByStatus(status);
	}

	@Override
	public Double getAvgPriceCostFromAllBookings() {
		return bookingRepository.getAvgPriceCostFromAllBookings();
	}

	@Override
	public BigDecimal getMinPriceCostFromAllBookings() {
		return bookingRepository.getMinPriceCostFromAllBookings();
	}

	@Override
	public BigDecimal getMaxPriceCostFromAllBookings() {
		return bookingRepository.getMaxPriceCostFromAllBookings();
	}

	@Override
	public List<Booking> getAllBookingsByPartner(String companyName) {
		return bookingRepository.getAllBookingsByPartner(companyName);
	}

	@Override
	public List<Booking> getAllBookingsByStatusByPartner(String status, String companyName) {
		return bookingRepository.getAllBookingsByStatusByPartner(status,companyName);
	}

	@Override
	public Double getAvgPriceCostFromAllBookingsByPartner(String companyName) {
		return bookingRepository.getAvgPriceCostFromAllBookingsByPartner(companyName);
	}

	@Override
	public BigDecimal getMinPriceCostFromAllBookingsByPartner(String companyName) {
		return bookingRepository.getMinPriceCostFromAllBookingsByPartner(companyName);
	}

	@Override
	public BigDecimal getMaxPriceCostFromAllBookingsByPartner(String companyName) {
		return bookingRepository.getMaxPriceCostFromAllBookingsByPartner(companyName);
	}
}
