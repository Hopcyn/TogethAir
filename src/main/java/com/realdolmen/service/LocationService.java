package com.realdolmen.service;

import com.realdolmen.domain.Location;
import com.realdolmen.remoteBean.LocationServiceRemote;
import com.realdolmen.repository.LocationRepository;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;


import java.io.Serializable;
import java.util.List;

public class LocationService implements LocationServiceRemote, Serializable {

	@EJB
	LocationRepository locationRepository;

	@Override
	public List<Location> getAllLocations() {
		return locationRepository.getAllLocations();
	}

	@Override
	public List<Location> getAllActiveLocations() {
		return this.locationRepository.getAllActiveLocations();
	}

	@Override
	public void persist(Location location) {
		this.locationRepository.persist(location);
	}

	@Override
	public void merge(Location location) {
		this.locationRepository.merge(location);
	}

	@Override
	public Location getLocationByCode(String code) {
		return locationRepository.getLocationByCode(code);
	}
}
