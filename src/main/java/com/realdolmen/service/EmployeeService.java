package com.realdolmen.service;

import com.realdolmen.domain.Employee;
import com.realdolmen.remoteBean.EmployeeServiceRemote;
import com.realdolmen.repository.EmployeeRepository;

import javax.ejb.EJB;
import java.io.Serializable;
import java.util.List;

public class EmployeeService implements EmployeeServiceRemote, Serializable {

	@EJB
	EmployeeRepository employeeRepository;

	@Override
	public List<Employee> getAllEmployees() {
		return employeeRepository.getAllEmployees();
	}
}
