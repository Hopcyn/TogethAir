package com.realdolmen.scheduled;

import com.realdolmen.domain.Booking;
import com.realdolmen.service.BookingService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class BookingList {
	private List<Booking> list;

	public BookingList() {
		list = new ArrayList<>();
	}

	public List<Booking> getList() {
		return list;
	}

	public void setList(List<Booking> list) {
		this.list = list;
	}
}
