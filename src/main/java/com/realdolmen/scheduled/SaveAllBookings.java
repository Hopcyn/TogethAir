package com.realdolmen.scheduled;

import com.realdolmen.domain.Booking;
import com.realdolmen.service.BookingService;
import com.thoughtworks.xstream.XStream;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Startup
@Singleton
public class SaveAllBookings {

	@Inject
	BookingService bookingService;

	@Schedule(second="*", minute="*", hour="1",persistent = false)
	public void saveBookings(Timer timer) {
		XStream xstream = new XStream();
		xstream.alias("booking", Booking.class);
		xstream.alias("Bookings", BookingList.class);
		xstream.addImplicitCollection(BookingList.class, "list");

		BookingList list = new BookingList();
		list.setList(bookingService.getAllBookings());

		String xml = xstream.toXML(list);
		System.out.println("XML Printed");



		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\PDF\\bookingLog.xml"));
			writer.write(xml);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
