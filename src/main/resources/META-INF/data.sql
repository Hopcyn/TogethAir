insert into country(id,globalRegion,name) values(1,"CENTRAL_EUROPE","Belgie");
insert into country(id,globalRegion,name) values(2,"CENTRAL_EUROPE","Nederland");
insert into country(id,globalRegion,name) values(3,"CENTRAL_EUROPE","Duitsland");
insert into country(id,globalRegion,name) values(4,"CENTRAL_EUROPE","Frankrijk");
insert into country(id,globalRegion,name) values(6,"NORTH_AMERICA","USA");
insert into country(id,globalRegion,name) values(7,"NORTH_AMERICA","Canada");


insert into location(id,airportName,airportCode,enabled,country_id) values(1,"Zaventem","BRU",true,1);
insert into location(id,airportName,airportCode,enabled,country_id) values(2,"Charleroi","CRL",true,1);
insert into location(id,airportName,airportCode,enabled,country_id) values(3,"Keulen","CGN",true,3);
insert into location(id,airportName,airportCode,enabled,country_id) values(4,"Schiphol","AMS",true,2);
insert into location(id,airportName,airportCode,enabled,country_id) values(5,"Parijs","PRS",true,4);
insert into location(id,airportName,airportCode,enabled,country_id) values(6,"Los Angeles","LAX",true,6);
insert into location(id,airportName,airportCode,enabled,country_id) values(7,"Toronto","TOR",true,6);

insert into user(id,emailAddress,enabled,firstName,lastName,password, USER_ROLE) values(1,"thijs@hotmail.com",true,"sam","leirens","$2a$11$RcvkwUGEIrAgSPdJOI4VmemeKwghtoKbnWF74wv/Raq.fAKbV/Wf6", "CUSTOMER");
insert into user(id,emailAddress,enabled,firstName,lastName,password, USER_ROLE) values(2,"steven@hotmail.com",true,"steven","leirens","$2a$11$RcvkwUGEIrAgSPdJOI4VmemeKwghtoKbnWF74wv/Raq.fAKbV/Wf6", "CUSTOMER");

insert into user(id,emailAddress,enabled,firstName,lastName,password,companyName, USER_ROLE) values(3,"sam@hotmail.com",true,"sam","leirens","$2a$11$RcvkwUGEIrAgSPdJOI4VmemeKwghtoKbnWF74wv/Raq.fAKbV/Wf6","TUI", "PARTNER");

insert into user(id,emailAddress,enabled,firstName,lastName,password, USER_ROLE) values(4,"robin@hotmail.com",true,"robin","couck","$2a$11$RcvkwUGEIrAgSPdJOI4VmemeKwghtoKbnWF74wv/Raq.fAKbV/Wf6", "EMPLOYEE");
insert into user(id,emailAddress,enabled,firstName,lastName,password,companyName, USER_ROLE) values(5,"neckerman@hotmail.com",true,"sam","leirens","$2a$11$RcvkwUGEIrAgSPdJOI4VmemeKwghtoKbnWF74wv/Raq.fAKbV/Wf6","NECKERMAN", "PARTNER");


insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(1,"ECONOMY_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(2,"BUSINESS_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(3,"FIRST_CLASS",50,50,10,50);

insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(4,"ECONOMY_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(5,"BUSINESS_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(6,"FIRST_CLASS",50,50,10,50);


insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(7,"ECONOMY_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(8,"BUSINESS_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(9,"FIRST_CLASS",50,50,10,50);


insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(10,"ECONOMY_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(11,"BUSINESS_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(12,"FIRST_CLASS",50,50,10,50);

insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(13,"ECONOMY_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(14,"BUSINESS_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(15,"FIRST_CLASS",50,50,10,50);


insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(16,"ECONOMY_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(17,"BUSINESS_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(18,"FIRST_CLASS",50,50,10,50);

insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(19,"ECONOMY_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(20,"BUSINESS_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(21,"FIRST_CLASS",50,50,10,50);


insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(22,"ECONOMY_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(23,"BUSINESS_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(24,"FIRST_CLASS",50,50,10,50);


insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(25,"ECONOMY_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(26,"BUSINESS_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(27,"FIRST_CLASS",50,50,10,50);


insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(28,"ECONOMY_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(29,"BUSINESS_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(30,"FIRST_CLASS",50,50,10,50);


insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(31,"ECONOMY_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(32,"BUSINESS_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(33,"FIRST_CLASS",50,50,10,50);

insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(34,"ECONOMY_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(35,"BUSINESS_CLASS",50,50,10,50);
insert into flightclass(id,FLIGHT_CLASS,amountOfSeats,basePrice,profitMargin, maxAmountOfSeats) values(36,"FIRST_CLASS",50,50,10,50);



insert into Discount(id,amountOfSeats,percentage,flight_id) values (1,5,10,1);
insert into Discount(id,amountOfSeats,percentage,flight_id) values (2,5,10,2);
insert into Discount(id,amountOfSeats,percentage,flight_id) values (3,5,10,3);
insert into Discount(id,amountOfSeats,percentage,flight_id) values (4,5,10,4);
insert into Discount(id,amountOfSeats,percentage,flight_id) values (5,5,10,5);
insert into Discount(id,amountOfSeats,percentage,flight_id) values (6,5,10,6);
insert into Discount(id,amountOfSeats,percentage,flight_id) values (7,5,10,7);
insert into Discount(id,amountOfSeats,percentage,flight_id) values (8,5,10,8);
insert into Discount(id,amountOfSeats,percentage,flight_id) values (9,3,5,1);




INSERT INTO flight (id,departureTime,duration,enabled,businessClass_id,departure_id,destination_id,economyClass_id,firstClass_id,partner_id) VALUES (1,{ts '2018-09-20 11:00:00.'},{t '04:30:45'},1,2,1,4,1,3,5);
INSERT INTO flight (id,departureTime,duration,enabled,businessClass_id,departure_id,destination_id,economyClass_id,firstClass_id,partner_id) VALUES (2,{ts '2018-09-20 12:00:00.'},{t '04:30:45'},1,5,1,4,4,6,3);
INSERT INTO flight (id,departureTime,duration,enabled,businessClass_id,departure_id,destination_id,economyClass_id,firstClass_id,partner_id) VALUES (3,{ts '2018-09-20 14:00:00.'},{t '04:30:45'},1,8,1,4,7,9,3);
INSERT INTO flight (id,departureTime,duration,enabled,businessClass_id,departure_id,destination_id,economyClass_id,firstClass_id,partner_id) VALUES (4,{ts '2018-09-20 16:00:00.'},{t '04:30:45'},1,11,1,4,10,12,3);
INSERT INTO flight (id,departureTime,duration,enabled,businessClass_id,departure_id,destination_id,economyClass_id,firstClass_id,partner_id) VALUES (5,{ts '2018-09-20 16:00:00.'},{t '04:30:45'},1,14,2,4,13,15,3);
INSERT INTO flight (id,departureTime,duration,enabled,businessClass_id,departure_id,destination_id,economyClass_id,firstClass_id,partner_id) VALUES (6,{ts '2018-09-21 13:00:00.'},{t '04:30:45'},1,17,1,4,16,18,3);
INSERT INTO flight (id,departureTime,duration,enabled,businessClass_id,departure_id,destination_id,economyClass_id,firstClass_id,partner_id) VALUES (7,{ts '2018-09-21 10:00:00.'},{t '04:30:45'},1,20,1,4,19,21,3);
INSERT INTO flight (id,departureTime,duration,enabled,businessClass_id,departure_id,destination_id,economyClass_id,firstClass_id,partner_id) VALUES (8,{ts '2018-09-21 12:00:00.'},{t '04:30:45'},1,23,1,4,22,24,3);
INSERT INTO flight (id,departureTime,duration,enabled,businessClass_id,departure_id,destination_id,economyClass_id,firstClass_id,partner_id) VALUES (9,{ts '2018-09-22 13:00:00.'},{t '04:30:45'},1,26,1,4,25,27,3);
INSERT INTO flight (id,departureTime,duration,enabled,businessClass_id,departure_id,destination_id,economyClass_id,firstClass_id,partner_id) VALUES (10,{ts '2018-09-22 16:00:00.'},{t '04:30:45'},1,29,1,4,28,30,3);
INSERT INTO flight (id,departureTime,duration,enabled,businessClass_id,departure_id,destination_id,economyClass_id,firstClass_id,partner_id) VALUES (11,{ts '2018-09-22 16:00:00.'},{t '01:30:00'},1,32,1,5,31,33,5);
INSERT INTO flight (id,departureTime,duration,enabled,businessClass_id,departure_id,destination_id,economyClass_id,firstClass_id,partner_id) VALUES (12,{ts '2018-09-23 16:00:00.'},{t '01:30:00'},1,35,1,5,34,36,3);


INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (1,5,1,'CreditCard','FINISHED',300.00,'firstClass',11,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (2,5,1,'Endorsement','TO_PAY',300.00,'firstClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (5,3,1,'Endorsement','CONFIRMED',144.00,'firstClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (6,1,1,'Endorsement','CONFIRMED',60.00,'firstClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (7,1,1,'Endorsement','CONFIRMED',60.00,'firstClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (8,3,1,'Endorsement','CONFIRMED',144.00,'firstClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (9,1,1,'Endorsement','CONFIRMED',60.00,'firstClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (10,3,1,'Endorsement','CONFIRMED',180.00,'firstClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (11,1,1,'Endorsement','CONFIRMED',60.00,'firstClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (12,3,1,'Endorsement','CONFIRMED',180.00,'businessClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (13,3,1,'Endorsement','CONFIRMED',180.00,'firstClass',3,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (14,1,1,'Endorsement','CONFIRMED',60.00,'firstClass',3,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (15,1,1,'CreditCard','CONFIRMED',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (17,3,1,'Endorsement','CONFIRMED',144.00,'firstClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (18,1,1,'Endorsement','CONFIRMED',48.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (20,5,1,'Endorsement','CONFIRMED',270.00,'firstClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (21,3,1,'Endorsement','CONFIRMED',144.00,'firstClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (22,3,1,'Endorsement','CONFIRMED',144.00,'economyClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (23,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',4,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (24,5,1,'Endorsement','CONFIRMED',270.00,'businessClass',3,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (25,3,1,'Endorsement','CONFIRMED',144.00,'firstClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (26,5,1,'Endorsement','CONFIRMED',270.00,'economyClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (27,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (28,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (30,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (31,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (32,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (33,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (34,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (35,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (36,1,1,'Endorsement','CONFIRMED',60.00,'economyClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (37,1,1,'Endorsement','CONFIRMED',60.00,'firstClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (38,1,1,'Endorsement','CONFIRMED',60.00,'economyClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (39,1,1,'Endorsement','CONFIRMED',60.00,'economyClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (40,3,1,'Endorsement','CONFIRMED',180.00,'economyClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (41,1,1,'Endorsement','CONFIRMED',60.00,'economyClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (42,1,1,'Endorsement','CONFIRMED',60.00,'economyClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (43,1,1,'Endorsement','CONFIRMED',60.00,'firstClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (44,1,1,'Endorsement','CONFIRMED',60.00,'firstClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (45,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (46,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (47,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (48,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (49,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (50,1,1,'Endorsement','CONFIRMED',60.00,'economyClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (51,2,1,'Endorsement','CONFIRMED',120.00,'economyClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (52,1,1,'Endorsement','CONFIRMED',60.00,'economyClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (53,1,1,'Endorsement','CONFIRMED',60.00,'firstClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (54,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (55,1,1,'Endorsement','CONFIRMED',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (56,1,1,'Endorsement','PENDING',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (57,1,1,'Endorsement','PENDING',60.00,'economyClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (58,1,1,'Endorsement','PENDING',60.00,'economyClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (59,5,1,'Endorsement','PENDING',270.00,'economyClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (60,1,1,'Endorsement','PENDING',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (61,1,1,'Endorsement','PENDING',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (62,1,1,'Endorsement','PENDING',60.00,'firstClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (63,1,1,'Endorsement','PENDING',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (64,1,1,'Endorsement','PENDING',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (65,1,1,'Endorsement','PENDING',60.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (66,5,1,'Endorsement','PENDING',270.00,'businessClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (67,5,1,'Endorsement','PENDING',270.00,'businessClass',1,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (68,2,1,'Endorsement','PENDING',120.00,'businessClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (69,7,1,'Endorsement','PENDING',378.00,'businessClass',2,3);
INSERT INTO booking (id,amountOfSeats,enabled,paymentMethodType,status,totalPrice,travelingClass,flight_id,user_id) VALUES (70,9,1,'Endorsement','PENDING',486.00,'firstClass',2,3);
=======
insert into Discount(id,amountOfSeats,percentage,flight_id) values (10,10,15,8);
insert into Discount(id,amountOfSeats,percentage,flight_id) values (11,10,15,1);

>>>>>>> ad906499d22ba7a384e300a29aedee3f70419734
